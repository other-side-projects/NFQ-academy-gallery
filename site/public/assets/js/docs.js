/*--------------------------------------
 * Only used for documentation!
 --------------------------------------*/
!function ($) {
    $(function () {

        /* Apply styles to upload fields
         ---------------------------------------------------------------------------------------- */

        $('input[type=file]').bootstrapFileInput();

        /* styleswitch
         ---------------------------------------------------------------------------------------- */
        /*if ('localStorage' in window && window['localStorage'] !== null) {
         if (localStorage.getItem('target') !== null) {
         $("#target").attr("href", "dist/css/bootstrap.min." + localStorage.getItem('target') + ".css");
         }
         }

         $('.styleswitch').click(function() {
         var $_this = $(this);
         if ('localStorage' in window && window['localStorage'] !== null) {
         localStorage.setItem('target', $(this).attr('rel'));
         $("#target").attr("href", "dist/css/bootstrap.min." + localStorage.getItem('target') + ".css");
         }
         return false;
         });*/

        /* styleshwitch tooltip
         ---------------------------------------------------------------------------------------- */
        $('.color_tooltip').tooltip();

        /* from original docs: demo code
         ---------------------------------------------------------------------------------------- */
        // tooltip demo
        $('.tooltip-demo').tooltip({
            selector: "[data-toggle=tooltip]",
            container: "body"
        })

        $('.tooltip-test').tooltip()
        $('.popover-test').popover()

        $('.bs-docs-navbar').tooltip({
            selector: "a[data-toggle=tooltip]",
            container: ".bs-docs-navbar .nav"
        })

        // popover demo
        $("[data-toggle=popover]")
            .popover()

        // button state demo
        $('#fat-btn')
            .click(function () {
                var btn = $(this)
                btn.button('loading')
                setTimeout(function () {
                    btn.button('reset')
                }, 3000)
            })

        // carousel demo
        $('.bs-docs-carousel-example').carousel()


        /* fuel ux spinner
         ---------------------------------------------------------------------------------------- */
        if ($('#ex-spinner')) {
            $('#ex-spinner').spinner('value', 25);
        }

        /* fuel ux tree
         ---------------------------------------------------------------------------------------- */
        var dataSourceTree = new DataSourceTree({
            data: [
                { name: 'Test Folder 1', type: 'folder', additionalParameters: { id: 'F1' } },
                { name: 'Test Folder 2', type: 'folder', additionalParameters: { id: 'F2' } },
                { name: 'Test Item 1', type: 'item', additionalParameters: { id: 'I1' } },
                { name: 'Test Item 2', type: 'item', additionalParameters: { id: 'I2' } }
            ],
            delay: 400
        });

        $('#ex-tree').tree({
            dataSource: dataSourceTree,
            loadingHTML: '<div class="static-loader">Loading...</div>',
            multiSelect: true,
            cacheItems: true
        });

        /* datetime picker
         ---------------------------------------------------------- */
        $('#datetimepicker1').datetimepicker({
            language: 'en',
            pick12HourFormat: true
        });

        $('#datetimepicker2').datetimepicker({
            pickDate: false
        });

        $('#datetimepicker3').datetimepicker({
            pickTime: false
        });

        /* date range picker
         ---------------------------------------------------------- */
        $('#reservation').daterangepicker();

        $('#reservationtime').daterangepicker({
            timePicker: true,
            timePickerIncrement: 30,
            format: 'MM/DD/YYYY h:mm A'
        });

        $('#reportrange').daterangepicker(
            {
                startDate: moment().subtract('days', 29),
                endDate: moment(),
                minDate: '01/01/2012',
                maxDate: '12/31/2014',
                dateLimit: { days: 60 },
                showDropdowns: true,
                showWeekNumbers: true,
                timePicker: false,
                timePickerIncrement: 1,
                timePicker12Hour: true,
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
                    'Last 7 Days': [moment().subtract('days', 6), moment()],
                    'Last 30 Days': [moment().subtract('days', 29), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
                },
                opens: 'left',
                buttonClasses: ['btn btn-default'],
                applyClass: 'btn-small btn-primary',
                cancelClass: 'btn-small',
                format: 'MM/DD/YYYY',
                separator: ' to ',
                locale: {
                    applyLabel: 'Submit',
                    fromLabel: 'From',
                    toLabel: 'To',
                    customRangeLabel: 'Custom Range',
                    daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                    monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                    firstDay: 1
                }
            },
            function (start, end) {
                console.log("Callback has been called!");
                $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            }
        );
        //Set the initial state of the picker label
        $('#reportrange span').html(moment().subtract('days', 29).format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));

        /* colorpicker
         ---------------------------------------------------------- */
        $('#cp1').colorpicker({
            format: 'hex'
        });
        $('#cp2').colorpicker();
        $('#cp3').colorpicker();
        var bodyStyle = $('body')[0].style;
        $('#cp4').colorpicker().on('changeColor', function (ev) {
            bodyStyle.backgroundColor = ev.color.toHex();
        });

        /* google maps
         ---------------------------------------------------------- */
        var myCenter = new google.maps.LatLng(53, -1.33);

        var marker = new google.maps.Marker({
            position: myCenter,
            url: '/',
            animation: google.maps.Animation.DROP
        });

        function initialize() {
            var mapProp = {
                center: myCenter,
                zoom: 13,
                draggable: true,
                scrollwheel: false,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };

            var map = new google.maps.Map(document.getElementById("map-canvas"), mapProp);

            marker.setMap(map);
        }

        google.maps.event.addDomListener(window, 'load', initialize);
        //google.maps.event.addListener(marker, 'click', function() {window.location.href = marker.url;});
    });
}(window.jQuery)