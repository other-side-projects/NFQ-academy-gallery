    $( ".thumbnail" )
        .mouseenter(function() {
            $(this).find('.caption').removeClass("fadeOutUp").addClass("fadeInDown").show();
        })
        .mouseleave(function() {
            $(this).find('.caption').removeClass("fadeInDown").addClass("fadeOutUp");
        });

    $('.like, .unlike').click(function(evt){
        var $this = $(this);
        var href = $(this).attr('href');
        href = href.replace('/store/', '/astore/')
                   .replace('/destroy/', '/adestroy/');
        if ($this.hasClass('like')) {
            $.get(href, function (data) {
                $this.text('Un-Like ('+data.likes+')');
            }, "json" );
            href = href.replace('store','destroy');
        } else if ($this.hasClass('unlike')) {
            $.get(href, function (data) {
                $this.text('Like ('+data.likes+')');
            }, "json" );
            href = href.replace('destroy','store');
        }
        href = href.replace('/astore/', '/store/')
            .replace('/adestroy/', '/destroy/');
        $(this).attr('href',href);
        $this.toggleClass("like unlike");
        return false;
    });