<?php
/**
 * Class Like
 */
class Like extends Eloquent
{
    public $timestamps=false;
    protected $table = 'likes';

    /**
     * Eloquent object of user
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('User');
    }

    /**
     * Eloquent object of photo
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function photo()
    {
        return $this->belongsTo('Photo');
    }

}