<?php

/**
 * Class Album
 */
class Album extends Eloquent
{
    protected $table = 'albums';

    /**
     * Eloquent object of main photo
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function main_photo()
    {
        return $this->belongsTo('Photo','main_photo_id');
        // actually hasOne, but have to use belongsTo, because foreign key is in this table.
    }

    /**
     * Eloquent object of user
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('User');
    }

    /**
     * Eloquent object of photos
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function photos()
    {
        return $this->hasMany('Photo');
    }

    /**
     * Eloquent object of category
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo('Category');
    }

    /**
     * When deleting albums delete all related photos
     *
     * @return bool|null
     */
    public function delete()
    {
        // delete all related photos
        $photos = Photo::where('album_id','=',$this->id)->get(array('id'))->toArray();
        foreach ($photos as $photo) {
            $photo_ids[] = $photo['id'];
        }
        Comment::whereIn('photo_id',$photo_ids)->delete();
        Like::whereIn('photo_id',$photo_ids)->delete();
        Photo::where('album_id','=',$this->id)->delete();
        return parent::delete();
    }
}