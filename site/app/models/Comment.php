<?php
/**
 * Class Comment
 */
class Comment extends Eloquent
{
    protected $table = 'comments';

    /**
     * Eloquent object of user
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('User');
    }

    /**
     * Eloquent object of photo
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function photo()
    {
        return $this->belongsTo('Photo');
    }

}