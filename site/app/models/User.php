<?php

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;

/**
 * Class User
 */
class User extends Eloquent implements UserInterface, RemindableInterface
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('password');

    /**
     * Get the unique identifier for the user.
     *
     * @return mixed
     */
    public function getAuthIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Get the password for the user.
     *
     * @return string
     */
    public function getAuthPassword()
    {
        return $this->password;
    }

    /**
     * Get the e-mail address where password reminders are sent.
     *
     * @return string
     */
    public function getReminderEmail()
    {
        return $this->email;
    }

    /**
     * Returns true if user is admin
     *
     * @return bool
     */
    public function isAdmin() {
        return $this->group_id == 2;
    }

    /**
     * Eloquent object of albums
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function albums()
    {
        return $this->hasMany('Album');
    }

    /**
     * Eloquent object of group
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function group()
    {
        return $this->belongsTo('Group');
    }

    /**
     * Eloquent object of likes
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function likes()
    {
       return $this->hasMany('Like');
    }

    /**
     * Gets all user liked photos IDs
     *
     * @return array
     */
    public function getLikedPhotosIds() {
        $likedPhotoIds = array();
        // select `photo_id` from `likes` where `user_id` = '1'
        $likes = Like::where('user_id','=', $this->id)->get(array('photo_id'));
        foreach ($likes as $like)
            $likedPhotoIds[] = $like->photo_id;
        return $likedPhotoIds;
    }

    /**
     * Eloquent object of comments
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function comments()
    {
        return $this->hasMany('Comment');
    }

}