<?php

/**
 * Class Category
 */
class Category extends Eloquent
{
    protected $table = 'categories';

    /**
     * Eloquent object of albums
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function albums()
    {
        return $this->hasMany('Album');
    }

}