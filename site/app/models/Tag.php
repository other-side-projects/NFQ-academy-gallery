<?php

/**
 * Class Tag
 */
class Tag extends Eloquent
{
    public $timestamps=false;
    protected $table = 'tags';

    /**
     * Eloquent object of photos
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function photos() {
        return $this->belongsToMany('Photo','photo_tag','tag_id','photo_id');
    }

    /**
     * Deletes all photo tags with ID
     *
     * @return bool|null
     */
    public function delete()
    {
        DB::delete('DELETE FROM photo_tag WHERE tag_id=?', array($this->id));
        return parent::delete();
    }
}