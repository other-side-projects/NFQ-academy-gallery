<?php

/**
 * Class Photo
 */
class Photo extends Eloquent
{
    protected $table = 'photos';

    /**
     * Generates list of photos grouped in albums
     *
     * @return array
     */
    static public function generateGroupedList() {
        $rPhotos = array();
        $rAlbums = array();
        $albums = Album::with('photos')->get();
        $i = 0;
        foreach ($albums as $album) {
            foreach ($album->photos as $photo) {
                $rPhotos[$photo->id] = $photo->title;
            }
            $rAlbums[$album->title] = $rPhotos;
            $i++;
            unset($rPhotos);
        }
        return $rAlbums;
    }

    /**
     * Eloquent object of main album
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function main_album()
    {
        return $this->hasOne('Album','main_photo_id');
    }

    /**
     * Eloquent object of album
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function album()
    {
        return $this->belongsTo('Album');
    }

    /**
     * Eloquent object of likes
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function likes()
    {
        return $this->hasMany('Like','photo_id');
    }

    /**
     * Eloquent object of comments
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function comments()
    {
        return $this->hasMany('Comment','photo_id');
    }

    /**
     * Eloquent object of tags
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function tags() {
        return $this->belongsToMany('Tag','photo_tag','photo_id','tag_id');
    }

    /**
     * Deletes all likes and comments when deleting photo
     *
     * @return bool|null
     */
    public function delete()
    {
        Like::where('photo_id','=',$this->id)->delete();
        Comment::where('photo_id','=',$this->id)->delete();
        return parent::delete();
    }

    /**
     * Saves thumbnails to folder and returns filename
     *
     * @param $file file input file
     * @return string
     */
    public function moveAndReturnFilename($file)
    {
        //dd($file);
        $fileInfo = pathinfo($file->getClientOriginalName());
        $filename = $this->_clear_name($fileInfo['filename']);
        $extension = $fileInfo['extension'];

        if (!$this->_validate_existance($filename, $extension)) {
            $file->move(public_path() . '/uploads/images/', $filename . '.' . $extension);
            File::copy(public_path() . '/uploads/images/' . $filename . '.' . $extension,
                public_path() . '/uploads/images/thumbs/' . $filename . '.' . $extension);
            File::copy(public_path() . '/uploads/images/' . $filename . '.' . $extension,
                public_path() . '/uploads/images/main_thumbs/' . $filename . '.' . $extension);
            //$file->move(public_path().'/uploads/images/thumbs/',$filename.'.'.$extension);
        } else {
            $i = 1;
            $filename_real = $filename;
            while ($this->_validate_existance($filename_real, $extension)) {
                $i++;
                $filename_real = $filename . "-" . $i;
            }
            $file->move(public_path() . '/uploads/images/', $filename_real . '.' . $extension);
            File::copy(public_path() . '/uploads/images/' . $filename_real . '.' . $extension,
                public_path() . '/uploads/images/thumbs/' . $filename_real . '.' . $extension);
            File::copy(public_path() . '/uploads/images/' . $filename_real . '.' . $extension,
                public_path() . '/uploads/images/main_thumbs/' . $filename_real . '.' . $extension);
            //$file->move(public_path().'/uploads/images/thumbs/',$filename_real.'.'.$extension);
            $filename = $filename_real;
        }

        $thumb = Image::make(public_path() . '/uploads/images/thumbs/' . $filename . '.' . $extension);
        $thumb->resizeCanvas(245, 180, 'center', false, '#fff');
        $thumb->save(public_path() . '/uploads/images/thumbs/' . $filename . '.' . $extension);
        $thumb = Image::make(public_path() . '/uploads/images/main_thumbs/' . $filename . '.' . $extension);
        $thumb->resizeCanvas(150, 150, 'center', false, '#fff');
        $thumb->save(public_path() . '/uploads/images/main_thumbs/' . $filename . '.' . $extension);
        return $filename . '.' . $extension;
    }

    /**
     * Replaces all "sensitive" chars with underscore
     *
     * @param $name string original name
     * @return string
     */
    private function _clear_name($name)
    {
        $name = preg_replace("/[^A-Z0-9._-]/i", "_", $name);
        return $name;
    }

    /**
     * Checks if file exists;
     *
     * @param $filename string file name
     * @param $extension string extension
     * @return bool
     */
    private function _validate_existance($filename, $extension)
    {
        return File::exists(public_path() . '/uploads/images/' . $filename . '.' . $extension);
    }

}