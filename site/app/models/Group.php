<?php

/**
 * Class Group
 */
class Group extends Eloquent
{
    protected $table = 'groups';

    /**
     * Eloquent object of users
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function users()
    {
        return $this->hasMany('User');
    }

}