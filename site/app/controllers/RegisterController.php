<?php

/**
 * Class RegisterController
 */
class RegisterController extends BaseController
{
    protected $restful = true;

    /**
     * Displays registration form
     *
     * @return \Illuminate\Http\RedirectResponse|View
     */
    public function getIndex()
    {
        if (Auth::check()) {
            return Redirect::to('/')->with('alert', 'Already logged in.');
        } else {
            return View::make('forms.register');
        }
    }

    /**
     * Handles registration info
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postIndex()
    {
            if (Auth::check()) {
                return Redirect::to('/')->with('alert', 'Already logged in.');
            } else {
            $validate = Validator::make(
                array(
                    'username' => Input::get('username'),
                    'password' => Input::get('password'),
                    'password_confirmation' => Input::get('password_confirmation'),
                ),
                array(
                    'username' => 'required|min:3|max:50|alpha_num|unique:users',
                    'password' => 'required|min:3|confirmed'
                )
            );
            if ($validate->passes()) {
                $user = new User;
                $user->username = Input::get('username');
                $user->password = Hash::make(Input::get('password'));
                $user->group_id = 1;
                $user->save();
                return Redirect::to('/login')->with('success', 'Successfully registered new user. Now you can log in.');
            } else {
                $messages = $validate->messages();
                return Redirect::to('/register')->with('danger', $messages->all())->withInput(Input::except(array('password','password_confirmation')));
            }
        }

    }
}