<?php

/**
 * Class AlbumController
 */
class AlbumController extends \BaseController
{

    /**
     * Main function for albums index
     *
     * @return View
     */
    public function index()
    {
        return View::make('index.albums')->with('albums', Album::with(array('user','main_photo'))->paginate(Config::get('app.albums_per_page')));
    }

    /**
     * Views album creation form
     *
     * @return View
     */
    public function create()
    {
        //
        return View::make('forms.create.album');
    }

    /**
     * Saves new album to the database
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store()
    {
        $validate = Validator::make(
            array(
                'title' => Input::get('title'),
                'description' => Input::get('description'),
                'long_description' => Input::get('long_description'),
                'place' => Input::get('place'),
                'main_photo' => Input::get('main_photo')
            ),
            array(
                'title' => 'required|min:3',
                'description' => 'required|min:3',
                'long_description' => 'min:3',
                'place' => 'required|min:3',
                'main_photo' => 'exists:photos,id'
            )
        );
        if ($validate->fails()) {
            $messages = $validate->messages();
            return Redirect::route('album.create')->with('danger', $messages->all())->withInput(Input::except('password'));
        }
        else {
            $album = new Album;
            $album->title = Input::get('title');
            $album->description = Input::get('description');
            $album->long_description = Input::get('long_description');
            $album->place = Input::get('place');
            $album->user_id = Auth::user()->id;
            if (Input::get('main_photo')!='') $album->main_photo_id = Input::get('main_photo');
            $album->save();

            return Redirect::route('album.index')->with('alarm', 'Album was successfully created');
        }

    }

    /**
     * Shows album by ID
     *
     * @param $id int Album id
     * @return View
     */
    public function show($id)
    {
        $data['album'] = Album::findOrFail($id);
        //select count(*) as aggregate from `photos` where `album_id` = '3'
        //select * from `photos` where `album_id` = '3' order by `id` desc limit 16 offset 0
        //select * from `likes` where `likes`.`photo_id` in ('44', '7', '2')
        $data['photos'] = Photo::with('likes')->where('album_id','=',$id)->orderBy('id', 'desc')->paginate(Config::get('app.photos_per_page'));
        if (Auth::check()) { $liked = Auth::user()->getLikedPhotosIds(); }
        else { $liked = array(); }
        return View::make('show.album')->with(array('data'=>$data,'liked'=>$liked));
    }

    /**
     * Shows album edit form by given ID
     *
     * @param $id int Album ID
     * @return View
     */
    public function edit($id)
    {
        $album = Album::findOrFail($id);
        return View::make('forms.edit.album')->with('album',$album);
    }

    /**
     * Saves submitted album info
     *
     * @param $id int album ID
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id)
    {
        $album = Album::findOrFail($id);

        $validate = Validator::make(
            array(
                'title' => Input::get('title'),
                'description' => Input::get('description'),
                'long_description' => Input::get('long_description'),
                'place' => Input::get('place'),
                'main_photo_id' => Input::get('main_photo')
            ),
            array(
                'title' => 'required|min:3',
                'description' => 'required|min:3',
                'long_description' => 'min:3',
                'place' => 'required|min:3',
                'main_photo_id' => 'exists:photos,id'
            )
        );
        if ($validate->fails()) {
            $messages = $validate->messages();
            return Redirect::route('album.edit',array('album'=>$id))->with('danger', $messages->all())->withInput(Input::all());
        }
        else {
            $album->title = Input::get('title');
            $album->description = Input::get('description');
            $album->long_description = Input::get('long_description');
            $album->place = Input::get('place');
            if (Input::get('main_photo_id')!='') $album->main_photo_id = Input::get('main_photo_id');
            $album->save();
            return Redirect::route('album.index')->with('alert', 'Album was successfully updated.');
        }
    }

    /**
     * Deletes album by given ID
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $album = Album::findOrFail($id);
        $album->delete();
        return Redirect::route('album.index')->with('success', 'Successfully deleted album with ID: ' . $id);
    }

}