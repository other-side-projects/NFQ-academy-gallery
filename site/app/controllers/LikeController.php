<?php

/**
 * Class LikeController
 */
class LikeController extends BaseController {
    public $restful=true;

    /**
     * Deletes like by given ID
     *
     * @param $id int photo ID
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getDestroy($id) {
        $photo = Photo::findOrFail($id);

        if (!Auth::check()) return Redirect::route('photo.show',array('photo'=>$id))->with('danger','Need to login first!');

        $like = Like::where('photo_id', '=', $id)->where('user_id', '=', Auth::user()->id);
        if ($like->count()>0) {
            $like->delete();
            DB::table('photos')->where('id','=',$photo->id)->decrement('total_likes');
            return Redirect::back()->with('success','You don\'t like this photo anymore.');
        } else {
            return Redirect::back()->with('alert','You don\'t like this photo.');
        }
        //return Response::json(array('id'=>$id));
    }

    /**
     * Creates like by given ID
     *
     * @param $id int photo ID
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getStore($id) {
        $photo = Photo::findOrFail($id);

        if (!Auth::check()) return Redirect::route('photo.show',array('photo'=>$id))->with('danger','Need to login first!');

        if (!Like::where('photo_id', '=', $id)->where('user_id', '=', Auth::user()->id)->exists()) {
            $newLike = new Like;
            $newLike->photo_id = $id;
            $newLike->user_id = Auth::user()->id;
            $newLike->save();
            DB::table('photos')->where('id','=',$photo->id)->increment('total_likes');
            return Redirect::back()->with('success','You like this photo.');
        } else {
            return Redirect::back()->with('alert','You already like this photo');
        }
         //return Response::json(array('id'=>$id));
    }

    public function getADestroy($id) {
        $photo = Photo::findOrFail($id);

        if (!Auth::check()) return Response::json(array('error'=>'You have no rights to do that'));

        $like = Like::where('photo_id', '=', $id)->where('user_id', '=', Auth::user()->id);
        if ($like->count()>0) {
            $like->delete();
            DB::table('photos')->where('id','=',$photo->id)->decrement('total_likes');
            return Response::json(array('success'=>'Disliked photo', 'likes'=>$photo->likes->count()));
        } else {
            return Response::json(array('error'=>'You do not like this photo. Nothing to delete.'));
        }
        //return Response::json(array('id'=>$id));
    }

    public function getAStore($id) {
        $photo = Photo::findOrFail($id);

        if (!Auth::check()) return Response::json(array('error'=>'You have no rights to do that'));

        if (!Like::where('photo_id', '=', $id)->where('user_id', '=', Auth::user()->id)->exists()) {
            $newLike = new Like;
            $newLike->photo_id = $id;
            $newLike->user_id = Auth::user()->id;
            $newLike->save();
            DB::table('photos')->where('id','=',$photo->id)->increment('total_likes');
            return Response::json(array('success'=>'You like this photo.', 'likes'=>$photo->likes->count()));
        } else {
            return Response::json(array('error'=>'You do not like this photo. Nothing to delete.'));
        }
        //return Response::json(array('id'=>$id));
    }
}