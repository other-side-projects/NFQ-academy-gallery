<?php

/**
 * Class CommentController
 */
class CommentController extends BaseController {
    public $restful=true;

    /**
     * Saves new comment by given ID
     *
     * @param $id int Photo ID
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postStore($id) {
        $photo = Photo::with(array('comments'))->findOrFail($id);

        if (!Auth::check()) return Redirect::route('photo.show',array('photo'=>$id))->with('danger',array('Need to login first!'));

        $validate = Validator::make(
            array(
                'comment'=>Input::get('comment')
            ),
            array(
                'comment'=>'required|max:500|min:5'
            )
        );
        if ($validate->fails()) {
            $messages = $validate->messages();
            return Redirect::route('photo.show',array('photo'=>$id))->with('danger', $messages->all())->withInput(Input::all());
        } else {
            $newComment = new Comment;
            $newComment->user_id = Auth::user()->id;
            $newComment->comment = Input::get('comment');
            $newComment->photo_id = $id;
            // insert into `comments` (`user_id`, `comment`, `photo_id`, `updated_at`, `created_at`) values ('1', 'Yeagaga', '44', '2013-12-05 21:07:54', '2013-12-05 21:07:54')
            $newComment->save();
            // update `photos` set `total_comments` = `total_comments` + 1 where `id` = '44'
            DB::table('photos')->where('id','=',$photo->id)->increment('total_comments');
            return Redirect::route('photo.show',array('photo'=>$id))->with('success','Your comment posted. Thanks.');
        }


        //return Response::json(array('id'=>$id));
    }

    /**
     * Generates list of comments in JSON
     *
     * @param $id int photo ID
     * @return \Illuminate\Http\JsonResponse
     */
    public function getList($id) {
        $photo = Photo::with(array('comments','comments.user'))->findOrFail($id);
        return Response::json($photo->comments);
    }

    /**
     * Deletes comment by given ID
     *
     * @param $id int Comment ID
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postDestroy($id) {
        if (!Auth::check()) return Redirect::back()->with('danger',array('Need to login first!'));

        $comment = Comment::findOrFail($id);

        if ((Auth::check() && Auth::user()->id == $comment->user_id) || (Auth::user()->isAdmin())) {
            //delete from `comments` where `id` = '65'
            $comment->delete();
            return Redirect::back()->with('alert','Your selected comment deleted.');
        } else {
            return Redirect::back()->with('danger', array('Sorry no rights to delete this photo'));
        }
    }
}