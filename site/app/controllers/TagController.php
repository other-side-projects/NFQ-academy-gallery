<?php

/**
 * Class TagController
 */
class TagController extends BaseController {
    public $restful=true;

    /**
     * Generates list with all tags and returns it in JSON
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getJson() {
        $tags_arr = array();
        // select `tag` from `tags`
        $tags = Tag::all(array('tag'));
        foreach ($tags as $tag) {
            $tags_arr[] = $tag['tag'];
        }
        return Response::json($tags_arr);
    }

    /**
     * Searches tags by given string
     *
     * @param string $tags
     * @return View|\Illuminate\Http\RedirectResponse
     */
    public function getSearch($tags=null) {
        if ($tags==null) $tags = Input::get('tags');
        if (!empty($tags)) {
            $validate = Validator::make(
                array ('tags'=>$tags),
                array('tags'=>'required|Regex:/^([a-z0-9\,])+$/i')
            );

            if ($validate->passes()) {
                //select `photos`.`id`, `photos`.`title`, `photos`.`file`, `photos`.`total_likes`, `photos`.`total_comments` from `photos`
                //inner join `photo_tag` on `photos`.`id` = `photo_tag`.`photo_id`
                //inner join `tags` on `photo_tag`.`tag_id` = `tags`.`id`
                //where `tags`.`tag` in ('?') group by `photos`.`id`
                $photos = DB::table('photos')
                            ->select(array('photos.id','photos.title','photos.file','photos.total_likes','photos.total_comments'))
                            ->join('photo_tag','photos.id','=','photo_tag.photo_id')
                            ->join('tags','photo_tag.tag_id','=','tags.id')
                            ->whereIn('tags.tag',explode(',',$tags),'or')
                            ->groupBy('photos.id')
                            ->paginate(Config::get('app.photos_per_page'));
                
                if ($photos==null) { return Redirect::action('TagController@getSearch')->with('danger',array('Sorry no photos found with given keywords')); }
                else {
                    if (Auth::check()) { $liked = Auth::user()->getLikedPhotosIds(); }
                    else { $liked = array(); }
                    return View::make('index.photos')->with(array('photos'=>$photos,'liked'=>$liked));
                }

            }
            else return Redirect::action('TagController@getSearch')->with('danger',array('sorry invalid request'))->withInput(Input::all());
        } else {
            return View::make('forms.search');
        }
        //return $tags;
    }
}