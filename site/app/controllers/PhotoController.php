<?php

class PhotoController extends \BaseController
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if (Auth::check()) { $liked = Auth::user()->getLikedPhotosIds(); }
        else { $liked = array(); }
        return View::make('index.photos')->
            with(array(
                    //select count(*) as aggregate from `photos`
                    //select * from `photos` order by `id` desc limit 16 offset 0
                    //select * from `albums` where `albums`.`id` in ('4', '6', '1')
                    //select * from `likes` where `likes`.`photo_id` in ('43', '42', '41', '40', '39', '38', '37', '36', '35', '34', '31', '30', '29', '27', '26', '25')
                    'photos'=> Photo::with(array('album','likes'))->orderBy('id', 'desc')->paginate(Config::get('app.photos_per_page'))
                ,'liked'=>$liked)

            );
        //dd(Photo::with('album')->get()->toArray());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param int $album
     * @return \Illuminate\Http\Response
     */
    public function create($album = null)
    {
        if ($album != '') {
            return Redirect::route('photo.create')->withInput(array('album' => $album));
        } else {
            return View::make('forms.create.photo');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        //
        $validate = Validator::make(
            array(
                'file' => Input::file('file'),
                'title' => Input::get('title'),
                'description' => Input::get('description'),
                'taken' => Input::get('taken'),
                'album' => Input::get('album'),
                'main' => Input::get('main'),
                'tags' => Input::get('tags')
            ),
            array(
                'file' => 'image|required',
                'title' => 'required|min:3|max:50',
                'description' => 'required|max:500',
                'taken' => 'required|date_format:"Y-m-d"',
                'album' => 'required|exists:albums,id',
                'main' => 'required|in:Yes,No',
                'tags' => 'required|Regex:/^([a-z0-9\,])+$/i'
            )
        );
        if ($validate->fails()) {
            $messages = $validate->messages();
            return Redirect::route('photo.create')->with('danger', $messages->all())->withInput(Input::except('file'));
        } else {
            $uploaded = Input::file('file');

            $photo = new Photo();
            $photo->title = Input::get('title');
            $photo->description = Input::get('description');
            $photo->album_id = Input::get('album');
            $photo->taken = Input::get('taken');
            $photo->file = $photo->moveAndReturnFilename($uploaded);
            //insert into `photos` (`title`, `description`, `album_id`, `taken`, `file`, `updated_at`, `created_at`) values ('asasdqwsdasd', 'Antras uploaded image', '3', '2013-10-25', 'wallpaper-1113694-13.jpg', '2013-12-05 21:04:04', '2013-12-05 21:04:04')
            $photo->save();
            // update `albums` set `total_photos` = `total_photos` + 1 where `id` = '3'
            DB::table('albums')->where('id','=',$photo->album_id)->increment('total_photos');
            // TAGS
            $tags = explode(',',Input::get('tags'));
            $db_tags = Tag::whereIn('tag',$tags,'or')->get();
            $db_tags1 = array();
            $tags_ids = array();
            foreach ($db_tags as $tag) {
                $db_tags1[] = $tag['tag'];
                $tags_ids[] = $tag['id'];
            }
            $new_tags = array_diff($tags,$db_tags1);
            foreach ($new_tags as $tag) {
                $newTag = new Tag;
                $newTag->tag = $tag;
                $newTag->save();
                $tags_ids[] = $newTag->id;
            }
            $photo->tags()->sync($tags_ids);
            // END TAGS
            if (Input::get('main')=="Yes") {
                $photo->album->main_photo_id = $photo->id;
                $photo->album->save();
            }
            return Redirect::route('photo.show',array('photo'=>$photo->id))->with('alert', 'Successfully uploaded picture.');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //select * from `photos` where `id` = '?' limit 1
        //select * from `likes` where `likes`.`photo_id` in ('3')
        //select `tags`.*, `photo_tag`.`photo_id` as `pivot_photo_id`, `photo_tag`.`tag_id` as `pivot_tag_id` from `tags` inner join `photo_tag` on `tags`.`id` = `photo_tag`.`tag_id` where `photo_tag`.`photo_id` in ('3')
        $photo = Photo::with('likes','tags')->findOrFail($id);
        //select * from `comments` where `photo_id` = '3' order by `id` desc limit 5 offset 0
        $comments = Comment::with('user')->where('photo_id','=',$id)->orderBy('id','desc')->paginate(Config::get('app.comments_per_page'));
        if (Auth::check()) { $liked = Auth::user()->getLikedPhotosIds(); }
        else { $liked = array(); }
        return View::make('show.photo')->with(array('photo'=>$photo,'comments'=>$comments,'liked'=>$liked));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //select * from `photos` where `id` = '43' limit 1
        //select `tags`.*, `photo_tag`.`photo_id` as `pivot_photo_id`, `photo_tag`.`tag_id` as `pivot_tag_id` from `tags` inner join `photo_tag` on `tags`.`id` = `photo_tag`.`tag_id` where `photo_tag`.`photo_id` in ('43')
        $photo = Photo::with('tags')->findOrFail($id);
        if (is_null(Input::get('tags'))) {
            $tags = '';
            foreach ($photo->tags as $tag) $tags .= $tag['tag'] . ',';
            $tags = trim($tags,',');
        } else {
            $tags = Input::get('tags');
        }
        return View::make('forms.edit.photo')->with(array('photo' => $photo, 'tags'=>$tags));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $photo = Photo::findOrFail($id);
        //update `albums` set `total_photos` = `total_photos` - 1 where `id` = '4'
        DB::table('albums')->where('id','=',$photo->album_id)->decrement('total_photos');
        $validate = Validator::make(
            array(
                'title' => Input::get('title'),
                'description' => Input::get('description'),
                'taken' => Input::get('taken'),
                'album' => Input::get('album'),
                'main' => Input::get('main'),
                'tags' => Input::get('tags')
            ),
            array(
                'title' => 'required|min:3|max:50',
                'description' => 'required|max:500',
                'taken' => 'required|date_format:"Y-m-d"',
                'album' => 'required|exists:albums,id',
                'main' => 'required|in:Yes,No',
                'tags' => 'required|Regex:/^([a-z0-9\,])+$/i'
            )
        );
        if ($validate->fails()) {
            $messages = $validate->messages();
            return Redirect::route('photo.edit',array('photo'=>$id))->with('danger', $messages->all())->withInput(Input::all());
        } else {
            $photo->title = Input::get('title');
            $photo->description = Input::get('description');
            $photo->album_id = Input::get('album');
            $photo->taken = Input::get('taken');
            $photo->save();
            // update `albums` set `total_photos` = `total_photos` + 1 where `id` = '4'
            DB::table('albums')->where('id','=',$photo->album_id)->increment('total_photos');
            // TAGS
            $tags = explode(',',Input::get('tags'));
            // select * from `tags` where `tag` in ('first')
            $db_tags = Tag::whereIn('tag',$tags,'or')->get();
            $db_tags1 = array();
            $tags_ids = array();
            foreach ($db_tags as $tag) {
                $db_tags1[] = $tag['tag'];
                $tags_ids[] = $tag['id'];
            }
            $new_tags = array_diff($tags,$db_tags1);
            foreach ($new_tags as $tag) {
                $newTag = new Tag;
                $newTag->tag = $tag;
                //insert into `tags` (`tag`) values ('asdwq')
                $newTag->save();
                $tags_ids[] = $newTag->id;
            }
            // insert into `photo_tag` (`photo_id`, `tag_id`) values ('44', '15')
            $photo->tags()->sync($tags_ids);
            // END TAGS
            if (Input::get('main')=="Yes") {
                $photo->album->main_photo_id = $photo->id;
                $photo->album->save();
            }
            return Redirect::route('photo.show',array('photo'=>$photo->id))->with('alert', 'Successfully updated picture.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $photo = Photo::findOrFail($id);
        DB::table('albums')->where('id','=',$photo->album_id)->decrement('total_photos');
        $photo->delete();
        //
        return Redirect::back()->with('alert','Photo successfully deleted');
    }

}