<?php

/**
 * Class LoginController
 */
class LoginController extends BaseController
{
    protected $restful = true;

    /**
     * Displays login form
     *
     * @return \Illuminate\Http\RedirectResponse|View
     */
    public function getIndex()
    {
        if (Auth::check()) {
            return Redirect::to('/')->with('alert', 'Already logged in.');
        } else {
            return View::make('forms.login');
        }
    }

    /**
     * Logs user in or not
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postIndex()
    {
        if (Auth::attempt(
            array(
                'username' => Input::get('username'),
                'password' => Input::get('password')
            ), Input::get('remember')
        )
        ) {
            return Redirect::to('/')->with('success', 'Successfully logged in.');
        } else {
            return Redirect::to('/login')->with('danger', array('Wrong username or password'))->withInput(Input::except(array('password')));
        }

    }
}