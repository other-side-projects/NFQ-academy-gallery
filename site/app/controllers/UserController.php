<?php

class UserController extends \BaseController
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return View::make('index.users')->with('users', User::with('albums')->paginate(Config::get('app.users_per_page')));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View::make('forms.create.user');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        //
        $validate = Validator::make(
            array(
                'username' => Input::get('username'),
                'password' => Input::get('password'),
                'group_id' => Input::get('group_id')
            ),
            array(
                'username' => 'required|min:3|max:50|alpha_num|unique:users',
                'password' => 'required|min:3',
                'group_id' => 'required|exists:groups,id'
            )
        );
        if ($validate->fails()) {
            $messages = $validate->messages();
            return Redirect::route('user.create')->with('danger', $messages->all())->withInput(Input::except('password'));
        }
        else {
            $user = new User();
            $user->username = Input::get('username');
            $user->password = Hash::make(Input::get('password'));
            $user->group_id = Input::get('group_id');
            $user->save();
            return Redirect::route('user.index')->with('alert','Successfully created new user');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $user = User::findOrFail($id);

        return View::make('forms.edit.user')->with('data', $user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $user = User::findOrFail($id);
        if (Input::get('username')!=$user->username) {
            $username_rule = 'required|min:3|max:50|alpha_num|unique:users';
        } else {
            $username_rule = 'required|min:3|max:50|alpha_num';
        }
        $validate = Validator::make(
            array(
                'username' => Input::get('username'),
                'password' => Input::get('password'),
                'group_id' => Input::get('group_id')
            ),
            array(
                'username' => $username_rule,
                'password' => 'min:3',
                'group_id' => 'required|exists:groups,id'
            )
        );

        if ($validate->fails()) {
            $messages = $validate->messages();
            return Redirect::route('user.edit',array('user'=>$id))->with('danger', $messages->all())->withInput(Input::except('password'));
        }
        else {
            $user->username = Input::get('username');
            $password = Input::get('password');
            if (!empty($password)) { $user->password = Hash::make($password); }
            $user->group_id = Input::get('group_id');
            $user->save();
            return Redirect::route('user.index')->with('alert','User successfully updated');
        }



    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id User id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $user = User::findOrFail($id);
        if ($id!=1) {
            $user->delete();
            return Redirect::route('user.index')->with('success', 'Successfully deleted user with ID: ' . $id);
        } else {
            return Redirect::route('user.index')->with('danger', 'Sorry you can\'t delete admin user');
        }
    }

}