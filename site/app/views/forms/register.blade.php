@extends('master')

@section('panel-heading')
Register form
@stop

@section('panel-body')
<div class="form-group">
    {{ Form::open() }}
    {{ Form::label('username', 'Your username:') }}
    {{ Form::text('username',null,array('class'=>'form-control','placeholder'=>'Your username')) }}
    {{ Form::label('password', 'Your password:') }}
    {{ Form::password('password',array('class'=>'form-control','placeholder'=>'Your password')) }}
    {{ Form::label('password_confirmation', 'Repeat password:') }}
    {{ Form::password('password_confirmation',array('class'=>'form-control','placeholder'=>'Repeat password')) }}
    <br/>

    <div class="centered">{{ Form::button('Register', array('class'=>'btn btn-default fueled-white',
        'type'=>'submit')) }}
    </div>
    {{ Form::close() }}
</div>
@stop