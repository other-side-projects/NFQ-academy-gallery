@extends('master')

@section('panel-heading')
Create new album
@stop

@section('panel-body')
{{ Form::open(array('route' => 'album.store')) }}
{{ Form::label('title', 'Album title:') }}
{{ Form::text('title',null,array('class'=>'form-control','placeholder'=>'Album title')) }}
{{ Form::label('description', 'Description:') }}
{{ Form::text('description',null,array('class'=>'form-control','placeholder'=>'Description')) }}
{{ Form::label('place', 'Place where picture was taken:') }}
{{ Form::text('place',null,array('class'=>'form-control','placeholder'=>'Kaunas, lithuania')) }}
{{ Form::label('long_description', 'Long description:') }}
{{ Form::textarea('long_description',null,array('class'=>'form-control','placeholder'=>'Long description')) }}
{{ Form::label('main_photo', 'Main photo?') }}
{{ Form::select('main_photo', array(''=>'--- none ---') + Photo::generateGroupedList(), null, array('class'=>'form-control input-sm') ) }}



<br/>

<div class="centered">{{ Form::button('Submit', array('class'=>'btn btn-default fueled-white',
    'type'=>'submit')) }}
</div>
{{ Form::close() }}
@stop