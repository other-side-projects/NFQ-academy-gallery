@extends('master')

@section('panel-heading')
Upload new photo
@stop

@section('panel-body')
{{ Form::open(array('route' => 'photo.store','files'=>true)) }}
<div class="centered"> {{ Form::file('file',array('class'=>'btn-default fueled-white')) }}</div>
{{ Form::label('title', 'Photo title:') }}
{{ Form::text('title',null,array('class'=>'form-control','placeholder'=>'Photo title')) }}
{{ Form::label('description', 'Description:') }}
{{ Form::text('description',null,array('class'=>'form-control','placeholder'=>'Description')) }}
{{ Form::label('taken', 'Date when taken:') }}
<div id="datetimepicker3" class="input-group date">
    {{ Form::text('taken',null,array('class'=>'form-control','placeholder'=>'Date taken', 'data-format'=>'yyyy-MM-dd'))
    }}
    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
</div>
{{ Form::label('album', 'In album?') }}
{{ Form::select('album', Album::lists('title','id'), null, array('class'=>'form-control input-sm') ) }}

{{ Form::label('tags', 'Tags? (select or type, seperate with space or comma)') }}<br />
{{ Form::text('tags', null,array('class'=>'select2-offscreen','id'=>'tags')) }}<br />

{{ Form::label('main', 'Make album main?') }}
{{ Form::radio('main','Yes',false) }} Yes
{{ Form::radio('main','No',true) }} No
<br/>

<div class="centered">{{ Form::submit('Submit', array('class'=>'btn btn-default fueled-white')) }}
</div>
{{ Form::close() }}
@stop