@extends('master')

@section('panel-heading')
Search photos by tags
@stop

@section('panel-body')
{{ Form::open(array('action'=>'TagController@getSearch','method'=>'get')) }}
{{ Form::label('tags', 'Tags? (select or type, seperate with space or comma)') }}<br />
{{ Form::text('tags', null,array('class'=>'select2-offscreen','id'=>'tags')) }}<br />
<div class="search-button">{{ Form::button('Search', array('class'=>'btn btn-default fueled-white',
    'type'=>'submit')) }}
</div>
{{ Form::close() }}
@stop