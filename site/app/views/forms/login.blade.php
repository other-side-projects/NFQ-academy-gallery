@extends('master')

@section('panel-heading')
Login form
@stop

@section('panel-body')
<div class="form-group">
    {{ Form::open() }}
    {{ Form::label('username', 'Your username:') }}
    {{ Form::text('username',null,array('class'=>'form-control','placeholder'=>'Your username')) }}
    {{ Form::label('password', 'Your password:') }}
    {{ Form::password('password',array('class'=>'form-control','placeholder'=>'Your password')) }}
    {{ Form::label('remember', 'Remember next time?') }}
    {{ Form::checkbox('remember',1,true) }}
    <br/>

    <div class="centered">{{ Form::button('Submit', array('class'=>'btn btn-default fueled-white',
        'type'=>'submit')) }}
    </div>
    {{ Form::close() }}
</div>
@stop