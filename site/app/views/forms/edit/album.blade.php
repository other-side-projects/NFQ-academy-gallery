@extends('master')

@section('panel-heading')
Edit album {{ e($album->title) }}
@stop

@section('panel-body')
{{ Form::open(array('route' => array('album.update',$album->id), 'method'=>'put')) }}
{{ Form::label('title', 'Album title:') }}
{{ Form::text('title',$album->title,array('class'=>'form-control','placeholder'=>'Album title')) }}
{{ Form::label('description', 'Description:') }}
{{ Form::text('description',$album->description,array('class'=>'form-control','placeholder'=>'Description')) }}
{{ Form::label('place', 'Place where picture was taken:') }}
{{ Form::text('place',$album->place,array('class'=>'form-control','placeholder'=>'Kaunas, lithuania')) }}
{{ Form::label('long_description', 'Long description:') }}
{{ Form::textarea('long_description',$album->long_description,array('class'=>'form-control','placeholder'=>'Long description')) }}
{{ Form::label('main_photo_id', 'Main photo?') }}
{{ Form::select('main_photo_id', [''=>'--- none ---'] + Photo::generateGroupedList(), $album->main_photo_id, array('class'=>'form-control input-sm') ) }}

<br/>

<div class="centered">{{ Form::button('Submit', array('class'=>'btn btn-default fueled-white',
    'type'=>'submit')) }}
</div>
{{ Form::close() }}
@stop