@extends('master')

@section('panel-heading')
Edit user {{ e($data->username) }}
@stop

@section('panel-body')

{{ Form::open(array('route' => array('user.update',$data->id),'method'=>'PUT'),null) }}
{{ Form::label('username', 'User Name:') }}
{{ Form::text('username',$data->username,array('class'=>'form-control','placeholder'=>'User Name')) }}
{{ Form::label('group_id', 'Group:') }}
{{ Form::select('group_id', Group::lists('title','id'), $data->group_id, array('class'=>'form-control input-sm') ) }}
{{ Form::label('password', 'Password:') }}
{{ Form::password('password',array('class'=>'form-control','placeholder'=>'Password')) }}

<br/>

<div class="centered">{{ Form::button('Submit', array('class'=>'btn btn-default fueled-white',
    'type'=>'submit')) }}
</div>
{{ Form::close() }}
@stop