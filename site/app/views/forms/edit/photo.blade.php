@extends('master')

@section('panel-heading')
Edit photo
@stop

@section('panel-body')
{{ Form::open(array('route' => array('photo.update',$photo->id),'method'=>'put')) }}
<div class="centered">
    <a href="{{ URL::route('photo.show',array('photo'=>$photo->id)) }}">
        <img src="{{ URL::to('/uploads/images/thumbs/'.e($photo->file)) }}"
             alt="{{ e($photo->title) }}">
    </a>
</div>
{{ Form::label('title', 'Photo title:') }}
{{ Form::text('title',$photo->title,array('class'=>'form-control','placeholder'=>'Photo title')) }}
{{ Form::label('description', 'Description:') }}
{{ Form::text('description',$photo->description,array('class'=>'form-control','placeholder'=>'Description')) }}
{{ Form::label('taken', 'Date when taken:') }}
<div id="datetimepicker3" class="input-group date">
    {{ Form::text('taken',$photo->taken,array('class'=>'form-control','placeholder'=>'Date taken', 'data-format'=>'yyyy-MM-dd'))
    }}
    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
</div>
{{ Form::label('album', 'In album?') }}
{{ Form::select('album', Album::lists('title','id'), $photo->album_id, array('class'=>'form-control input-sm') ) }}

{{ Form::label('tags', 'Tags? (select or type, seperate with space or comma)') }}<br />
{{ Form::text('tags', $tags,array('class'=>'select2-offscreen','id'=>'tags')) }}<br />

{{ Form::label('main', 'Make album main?') }}
{{ Form::radio('main','Yes',false) }} Yes
{{ Form::radio('main','No',true) }} No
<br/>

<div class="centered">{{ Form::submit('Submit', array('class'=>'btn btn-default fueled-white')) }}
</div>
{{ Form::close() }}
@stop