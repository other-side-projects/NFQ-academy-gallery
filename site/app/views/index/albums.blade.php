@extends('master')

@section('panel-heading')
All albums
<div class="righted">
    {{-- BEGIN ADD BUTTON --}}
    @if (Auth::check() && Auth::user()->isAdmin())
    {{ Form::open(array('class'=>'inlined','method'=>'get','action'=>array('AlbumController@create'))) }}
    {{ Form::button('<span class="glyphicon glyphicon-plus"></span> Add album',array('class'=>'btn
    btn-success','type'=>'submit')) }}
    {{ Form::close() }}
    @endif
    {{-- END ADD BUTTON --}}
</div>
@stop

@section('panel-body')
@if ($albums->count()>0)
@foreach ($albums as $album)
<div class="container well well-sm">
    <div class="row">
        @if (Auth::check())
        <div class="col-sm-10">
            @else
            <div class="col-sm-12">
                @endif
                <div class="media">
                    @if (is_object($album->main_photo))
                    <a href="/album/{{ $album->id }}" class="pull-left"><img src="/uploads/images/main_thumbs/{{ e($album->main_photo->file) }}"
                                                                             class="media-object"
                                                                             alt="{{ e($album->title) }}"
                                                                             width="150"
                                                                             height="150"></a>
                    @else
                    <a href="/album/{{ $album->id }}" class="pull-left"><img src="http://lorempixel.com/150/150/"
                                                                             class="media-object"
                                                                             alt="{{ e($album->title) }}"></a>
                    @endif

                    <div class="media-body">
                        {{ HTML::linkRoute('album.show', $album->title, array($album->id),
                        array('class'=>'album-head media-heading')) }}
                        <p>{{ e($album->long_description) }}</p>

                        <p>{{ e($album->place) }}</p>

                        <small>Total photos: {{ $album->total_photos }}</small>

                    </div>
                </div>
            </div>

            @if (Auth::check() && Auth::user()->isAdmin())
            <div class="col-sm-2 centered">
                {{-- BEGIN EDIT BUTTON --}}
                {{ Form::open(array('class'=>'inlined','method'=>'get','action'=>array('AlbumController@edit',
                $album->id))) }}
                {{ Form::submit('Edit',array('class'=>'btn btn-xs btn-warning')) }}
                {{ Form::close() }}
                {{-- END EDIT BUTTON --}}
                {{-- BEGIN DELETE BUTTON --}}
                {{
                Form::open(array('class'=>'inlined','method'=>'delete','action'=>array('AlbumController@destroy',
                $album->id))) }}
                {{ Form::submit('DELETE',array('class'=>'btn btn-xs btn-danger')) }}
                {{ Form::close() }}
                {{-- END DELETE BUTTON --}}
                <br/> <br/>
                {{ HTML::link('/photo/create/'.$album->id, 'Add photos', array('class'=>'btn btn-sm btn-primary')) }}
            </div>
            @endif
        </div>
    </div>
    @endforeach
    @else
    None albums are created yet
    @endif

    @stop

    @section('pagination')
    {{ $albums->links() }}
    @stop