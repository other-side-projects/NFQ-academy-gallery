@extends('master')

@section('panel-heading')
All photos
@stop

@section('panel-body')
@foreach ($photos as $photo)
<div class="col-sm-3 top-padding-10">
    <div class="thumbnail">
        <div class="caption animated">
            <p>{{ e($photo->title) }}</p>
            @if (Auth::check() && Auth::user()->isAdmin())
            {{ Form::open(array('route'=>array('photo.edit',$photo->id),'method'=>'GET','class'=>'inlined')) }}
                {{ Form::button('Edit',array('class'=>'btn btn-info btn-xs fueled-white','type'=>'submit')) }}
            {{ Form::close() }}
            {{ Form::open(array('route'=>array('photo.destroy',$photo->id),'method'=>'DELETE','class'=>'inlined')) }}
                {{ Form::button('Delete',array('class'=>'btn btn-danger btn-xs fueled-white','type'=>'submit')) }}
            {{ Form::close() }}
            @endif
            <a href="{{ URL::route('photo.show',array('photo'=>$photo->id)) }}" class="btn btn-success btn-xs fueled-white" rel="tooltip" title="View">View</a>
            @if (Auth::check())
                @if (!in_array($photo->id,$liked))
                <a href="{{ URL::action('LikeController@getStore',$photo->id) }}" class="btn btn-warning btn-xs fueled-white like" rel="tooltip" title="Like">Like ({{ $photo->total_likes }})</a>
                @else
                <a href="{{ URL::action('LikeController@getDestroy',$photo->id) }}" class="btn btn-warning btn-xs fueled-white unlike" rel="tooltip" title="Un-Like">Un-Like ({{ $photo->total_likes }})</a>
                @endif
            @else
            <a href="#" class="btn btn-warning btn-xs fueled-white" rel="tooltip" title="Like">Likes ({{ $photo->total_likes }})</a>
            @endif
        </div>
        <a href="{{ URL::route('photo.show',array('photo'=>$photo->id)) }}">
            <img src="{{ URL::to('/uploads/images/thumbs/'.e($photo->file)) }}"
                 class="photo-thumb-album" alt="{{ e($photo->title) }}">
        </a>
    </div>
</div>
@endforeach
@stop

@section('pagination')
{{ $photos->links() }}
@stop