@extends('master')

@section('panel-heading')
All Users
<div class="righted">
    {{-- BEGIN ADD BUTTON --}}
    {{ Form::open(array('class'=>'inlined','method'=>'get','action'=>array('UserController@create'))) }}
    {{ Form::button('<span class="glyphicon glyphicon-plus"></span> Add user',array('class'=>'btn
    btn-success','type'=>'submit')) }}
    {{ Form::close() }}
    {{-- END ADD BUTTON --}}
</div>
@stop

@section('panel-body')


<table class="table table-hover">
    <thead>
    <tr>
        <th>Username</th>
        <th>Date registered</th>
        <th>Actions</th>
    </tr>
    </thead>
    <tbody>
    @foreach ($users as $user)
    <tr>
        <td>{{ e($user->username) }}</td>
        <td>{{ e($user->created_at) }}</td>
        <td>
            {{-- BEGIN EDIT BUTTON --}}
            {{ Form::open(array('class'=>'inlined','method'=>'get','action'=>array('UserController@edit',
            $user->id))) }}
            {{ Form::submit('Edit',array('class'=>'btn btn-xs btn-warning')) }}
            {{ Form::close() }}
            {{-- END EDIT BUTTON --}}
            @if ($user->id != 1)
            {{-- BEGIN DELETE BUTTON --}}
            {{ Form::open(array('class'=>'inlined','method'=>'delete','action'=>array('UserController@destroy',
            $user->id))) }}
            {{ Form::submit('DELETE',array('class'=>'btn btn-xs btn-danger')) }}
            {{ Form::close() }}
            @endif
            {{-- END DELETE BUTTON --}}
        </td>
    </tr>
    @endforeach
    </tbody>
</table>


@stop