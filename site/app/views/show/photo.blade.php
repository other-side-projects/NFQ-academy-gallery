@extends('master')

@section('panel-heading')
{{ e($photo->title) }}
<div class="righted">
    {{-- BEGIN ADD BUTTON --}}
    @if (Auth::check() && Auth::user()->isAdmin())
    <a href="{{ URL::route('photo.edit',array('photo'=>$photo->id)) }}" class="btn btn-sm btn-warning"><span
            class="glyphicon glyphicon-pencil"></span> Edit photo</a>
    @endif
    {{-- END ADD BUTTON --}}
    @if (Auth::check())
        @if (!in_array($photo->id,$liked))
        <a href="{{ URL::action('LikeController@getStore',$photo->id) }}" class="btn btn-sm btn-info"><span
                class="glyphicon glyphicon-thumbs-up"></span> Like photo ({{ $photo->total_likes }})</a>
        @else
        <a href="{{ URL::action('LikeController@getDestroy',$photo->id) }}" class="btn btn-sm btn-info"><span
                class="glyphicon glyphicon-thumbs-down"></span> Un-Like photo ({{ $photo->total_likes }})</a>

        @endif
    @endif
</div>
@stop

@section('panel-body')
<div class="well well-sm">
    <p>{{ e($photo->description) }}</p>

    <p>Taken on: {{ e($photo->taken) }}</p>

    <p>Tags:
    @foreach ($photo->tags as $tag)
        <a href="/tag/search/{{ e($tag['tag']) }}" title="{{ e($tag['tag']) }}">{{ e($tag['tag']) }}</a>,
    @endforeach
    </p>
</div>
<a class="thumbnail" href="#">
    <img src="{{ URL::to('/uploads/images/'.e($photo->file)) }}" class="image-full-size"
         alt="{{ e($photo->title) }}">
</a>
@stop

@section('comments')
<div class="container">
    <div class="row">
        <div class="panel panel-default widget">
            <div class="panel-heading">
                <span class="glyphicon glyphicon-comment"></span>
                <h3 class="panel-title">
                    Recent Comments</h3>
                <span class="label label-info">
                    {{ $photo->total_comments }}</span>
            </div>
            <div class="panel-body">
                @if ($comments->count()>0)
                <ul class="list-group">
                    @foreach ($comments as $comment)
                    <li class="list-group-item">
                        <div class="row">
                            <div class="col-xs-12 col-md-12">
                                <div>
                                    <div class="mic-info">
                                        By: {{ e($comment->user->username) }} on {{ $comment->created_at }}
                                    </div>
                                </div>
                                <div class="comment-text">
                                    {{ e($comment->comment) }}
                                </div>
                                @if ((Auth::check() && Auth::user()->id==$comment->user_id) || Auth::user()->isAdmin())
                                <div class="action">
                                    {{ Form::open(array('action'=>array('CommentController@postDestroy',$comment->id),'method'=>'post','class'=>'inlined')) }}
                                        {{ Form::button('<span class="glyphicon glyphicon-trash"></span> Delete this comment',array('class'=>'btn btn-danger btn-xs fueled-white','type'=>'submit')) }}
                                    {{ Form::close() }}

                                </div>
                                @endif
                            </div>
                        </div>
                    </li>
                    @endforeach
                </ul>
                @else
                <div class="col col-sm-12 top-padding-10">
                    No comments yet. You could be first!
                </div>
                @endif
            </div>
            <div class="col col-sm-12 bottom-padding-10">
                @if (Auth::check())
                {{ Form::open(array('action' => array('CommentController@postStore', $photo->id))) }}

                {{ Form::textarea('comment',null,array('placeholder'=>'Type in your message','class'=>'bottom-margin-10 form-control counted','rows'=>5)) }}
                            <h6 class="pull-right" id="counter">320 characters remaining</h6>
                            <button class="btn btn-info" type="submit">Post New Message</button>
                {{ Form::close() }}
                @else
                <span class="label label-danger">You have to be logged in to comment.</span>
                @endif
            </div>

        </div>
        <div class="centered">{{ $comments->links() }}</div>
    </div>
</div>


@stop