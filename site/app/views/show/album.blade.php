@extends('master')

@section('panel-heading')
{{ e($data['album']->title) }}
<div class="righted">
    {{-- BEGIN ADD BUTTON --}}
    @if (Auth::check() && Auth::user()->isAdmin())
    <a href="/photo/create/{{ $data['album']->id }}" class="btn btn-sm btn-success"><span
            class="glyphicon glyphicon-plus"></span> Add photos</a>
    <a href="{{ URL::route('album.edit',array('album'=>$data['album']->id)) }}" class="btn btn-sm btn-warning"><span
            class="glyphicon glyphicon-pencil"></span> Edit album</a>
    @endif
    {{-- END ADD BUTTON --}}
</div>
@stop

@section('panel-body')
<div class="well well-sm">
    <p>{{ e($data['album']->long_description) }}</p>

    <p>{{ e($data['album']->place) }}</p>
</div>
<div class="row">
    @if (count($data['photos'])>0)
    @foreach ($data['photos'] as $photo)
    <div class="col-sm-3 top-padding-10">
        <div class="thumbnail">
            <div class="caption animated">
                <p>{{ e($photo->title) }}</p>
                @if (Auth::check() && Auth::user()->isAdmin())
                {{ Form::open(array('route'=>array('photo.edit',$photo->id),'method'=>'GET','class'=>'inlined')) }}
                {{ Form::button('Edit',array('class'=>'btn btn-info btn-xs fueled-white','type'=>'submit')) }}
                {{ Form::close() }}
                {{ Form::open(array('route'=>array('photo.destroy',$photo->id),'method'=>'DELETE','class'=>'inlined')) }}
                {{ Form::button('Delete',array('class'=>'btn btn-danger btn-xs fueled-white','type'=>'submit')) }}
                {{ Form::close() }}
                @endif
                <a href="{{ URL::route('photo.show',array('photo'=>$photo->id)) }}" class="btn btn-success btn-xs fueled-white" rel="tooltip" title="View">View</a>
                @if (Auth::check())
                @if (!in_array($photo->id,$liked))
                <a href="{{ URL::action('LikeController@getStore',$photo->id) }}" class="btn btn-warning btn-xs fueled-white like" rel="tooltip" title="Like">Like ({{ $photo->total_likes }})</a>
                @else
                <a href="{{ URL::action('LikeController@getDestroy',$photo->id) }}" class="btn btn-warning btn-xs fueled-white unlike" rel="tooltip" title="Un-Like">Un-Like ({{ $photo->total_likes }})</a>
                @endif
                @else
                <a href="#" class="btn btn-warning btn-xs fueled-white" rel="tooltip" title="Like">Likes ({{ $photo->total_likes }})</a>
                @endif
            </div>
            <a href="/photo/{{ $photo->id }}">
            <img src="{{ URL::to('/uploads/images/thumbs/'.e($photo->file)) }}"
                 class="photo-thumb-album" alt="{{ e($photo->title) }}">
            </a>
        </div>
    </div>
    @endforeach
    @else
    <div class="col-sm-12"><span class="text-warning">No photos found yet.</span>
    @endif
</div>
@stop

@section('pagination')
{{ $data['photos']->links() }}
@stop