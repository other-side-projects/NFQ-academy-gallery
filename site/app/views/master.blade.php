<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Justinas Bolys">

    <title>
        NFQgalerija.dev
    </title>

    <!-- Bootstrap core CSS -->
    {{ HTML::style('dist/css/bootstrap.min.matt-blue.css') }}
    {{ HTML::style('added/animate/animate.min.css') }}
    {{ HTML::style('added/custom.css') }}
    {{ HTML::style('added/select2/select2.css') }}
    {{ HTML::style('added/select2.css') }}

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->

    <!--[if lt IE 9]>
    {{ HTML::script('assets/js/html5shiv.js') }}
    {{ HTML::script('assets/js/respond.min.js') }}
    <![endif]-->

    <!-- Le Google Web Fonts -->
    {{ HTML::style('http://fonts.googleapis.com/css?family=Ropa+Sans') }}
    {{ HTML::style('http://fonts.googleapis.com/css?family=Archivo+Narrow:400,700') }}
</head>
<body>

<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button data-target=".header-collapse" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">NFQGalerija.dev</a>
        </div>
        <div class="collapse navbar-collapse header-collapse">
            <ul class="nav navbar-nav">
                <li class="dropdown">
                    <a href="javascript:" class="dropdown-toggle" data-toggle="dropdown">Photo <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        @if (Auth::check() && Auth::user()->isAdmin())
                        <li>
                            {{ HTML::linkRoute('photo.create', 'Upload photo') }}
                        </li>
                        @endif
                        <li>
                            {{ HTML::linkRoute('photo.index', 'List photos') }}
                        </li>
                        <li>
                            {{ HTML::linkAction('TagController@getSearch', 'Search photos by tags') }}
                        </li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="javascript:" class="dropdown-toggle" data-toggle="dropdown">Album <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        @if (Auth::check() && Auth::user()->isAdmin())
                        <li>
                            {{ HTML::linkRoute('album.create', 'Create album') }}
                        </li>
                        @endif
                        <li>
                            {{ HTML::linkRoute('album.index', 'List albums') }}
                        </li>
                    </ul>
                </li>

                @if (Auth::check() && Auth::user()->isAdmin())
                <li class="dropdown">
                <a href="javascript:" class="dropdown-toggle" data-toggle="dropdown">User <span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li>
                        {{ HTML::linkRoute('user.create', 'Create user') }}
                    </li>
                    <li>
                        {{ HTML::linkRoute('user.index', 'List users') }}
                    </li>
                </ul>
                </li>
                @endif

                @if (Auth::check())
                <li>
                    {{ HTML::linkRoute('logout', 'Logout') }}
                </li>
                @else
                <li>
                    {{ HTML::link('login', 'Login') }}
                </li>
                <li>
                    {{ HTML::link('register', 'Register') }}
                </li>
                @endif
            </ul>
        </div>
    </div>
</nav>

<main role="main" id="content">

    <div class="container">

        <section id="typography" class="panel panel-default">
            <div class="panel-heading">
                @yield('panel-heading')
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="panel-body">
                        @if (Session::get('success')!==null)
                        <div class="alert alert-success">{{ Session::get('success') }}</div>
                        @endif
                        @if (Session::get('alert')!==null)
                        <div class="alert alert-warning">{{ Session::get('alert') }}</div>
                        @endif
                        @if (Session::get('danger')!==null)
                        <div class="alert alert-danger">
                            @foreach (Session::get('danger') as $danger)
                            <ul>
                                {{ '
                                <li>' . $danger . '</li>
                                ' }}
                            </ul>
                            @endforeach
                        </div>
                        @endif
                        @yield('panel-body')
                    </div>
                </div>
                <div class="centered"> @yield('pagination') </div>
            </div>
        </section>

        @yield('comments')

        <hr>
        <footer>
            <div class="row">
                <div class="col-sm-12">
            <span class="pull-right">
                Fuelio Bootstrap 3 skin
            </span>

                    <p class="footer-pagragraph">Copyright &copy; Justinas Bolys &middot; 2013</p>
                </div>
            </div>
        </footer>
        <hr>


    </div>
</main>

<!-- JS and analytics only. -->
<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
{{ HTML::script('assets/js/jquery.js') }}
{{ HTML::script('dist/js/bootstrap.js') }}
{{ HTML::script('http://platform.twitter.com/widgets.js') }}
{{ HTML::script('assets/js/holder.js') }}
{{ HTML::script('added/datetimepicker/bootstrap-datetimepicker.js') }}
{{-- HTML::script('added/daterangepicker/moment.min.js') --}}
{{-- HTML::script('added/daterangepicker/daterangepicker.js') --}}
{{ HTML::script('added/colorpicker/js/bootstrap-colorpicker.js') }}
{{ HTML::script('added/input-mask/bootstrap-inputmask.min.js') }}
{{ HTML::script('added/file-upload/bootstrap-fileupload.min.js') }}
{{ HTML::script('added/fuel-ux/spinner.js') }}
{{ HTML::script('added/fuel-ux/checkbox.js') }}
{{ HTML::script('added/fuel-ux/radio.js') }}
{{ HTML::script('added/fuel-ux/combobox.js') }}
{{ HTML::script('added/fuel-ux/select.js') }}
{{ HTML::script('added/fuel-ux/pillbox.js') }}
{{ HTML::script('added/fuel-ux/tree.js') }}
{{ HTML::script('added/fuel-ux/require.js') }}
{{ HTML::script('added/fuel-ux/datasourceTree.js') }}
{{ HTML::script('added/fuel-ux/search.js') }}
{{ HTML::script('added/fuel-ux/wizard.js') }}
{{ HTML::script('added/typeahead/typeahead.js') }}
{{ HTML::script('added/file-input.js') }}
{{ HTML::script('assets/js/docs.js') }}
{{ HTML::script('added/custom.js') }}
{{ HTML::script('added/charCount.js') }}
{{ HTML::script('added/select2/select2.js') }}
{{ HTML::script('added/custom2.js') }}
</body>
</html>