<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', array('uses' => 'AlbumController@index'));

Route::group(array('before' => 'auth.admin'), function () {
    Route::resource('photo', 'PhotoController', array('except' => array('index', 'show')));
    Route::get('photo/create/{album}', 'PhotoController@create')->where('album', '[0-9]+');
    Route::resource('album', 'AlbumController', array('except' => array('index', 'show')));
    Route::resource('user', 'UserController');
});
Route::resource('photo', 'PhotoController', array('only' => array('index', 'show')));
Route::resource('album', 'AlbumController', array('only' => array('index', 'show')));

Route::get('logout', array('as' => 'logout', function () {
    Auth::logout();
    return Redirect::to('/')->with('success', 'Successfully logged out.');
}));
Route::controller('login', 'LoginController');
Route::controller('register', 'RegisterController');

Route::controller('comment', 'CommentController');
Route::controller('like', 'LikeController');

Route::controller('tag', 'TagController');