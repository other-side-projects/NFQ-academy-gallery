<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AlterTotalCommentsToPhotosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
    public function up()
    {
        Schema::table('photos', function(Blueprint $table) {
            $table->integer('total_comments')->default(0)->unsigned()->after('total_likes');
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('photos', function($table)
        {
            $table->dropColumn('total_comments');
        });
    }

}
