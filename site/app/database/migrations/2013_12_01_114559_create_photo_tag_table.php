<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePhotoTagTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('photo_tag', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('photo_id');
            $table->integer('tag_id');
			//$table->timestamps();
            $table->unique(array('photo_id','tag_id'));
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('photo_tag');
	}

}
