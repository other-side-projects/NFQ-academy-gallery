<?php

class UsersTableSeeder extends Seeder
{

    public function run()
    {
        // Uncomment the below to wipe the table clean before populating
        DB::table('users')->truncate();

        $users = array(
            array('username' => 'admin', 'password' => Hash::make('pass'), 'group_id'=>2),
            array('username' => 'user1', 'password' => Hash::make('pass2'), 'group_id'=>1),
            array('username' => 'user2', 'password' => Hash::make('pass3'), 'group_id'=>1)
        );

        User::insert($users);

        // Uncomment the below to run the seeder
        //DB::table('users')->insert($users);
    }

}
