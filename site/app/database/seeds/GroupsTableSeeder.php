<?php

class GroupsTableSeeder extends Seeder
{

    public function run()
    {
        // Uncomment the below to wipe the table clean before populating
        DB::table('groups')->truncate();

        $groups = array(
            array('id' => 1, 'title' => 'user'),
            array('id' => 2, 'title' => 'admin'),
        );

        Group::insert($groups);

        // Uncomment the below to run the seeder
        //DB::table('users')->insert($users);
    }

}
