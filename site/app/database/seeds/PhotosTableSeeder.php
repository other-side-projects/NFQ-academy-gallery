<?php

class PhotosTableSeeder extends Seeder
{

    public function run()
    {
        // Uncomment the below to wipe the table clean before populating
        DB::table('photos')->truncate();

        $photos = array(
            array('album_id' => 2,
                'title' => 'Photo title 1',
                'file' => 'file.jpg',
                'description' => 'Photo 1 description.'),
            array('album_id' => 3,
                'title' => 'Photo title 2',
                'file' => 'file.jpg',
                'description' => 'Photo 2 description.'),
            array('album_id' => 4,
                'title' => 'Photo title 3',
                'file' => 'file.jpg',
                'description' => 'Photo 3 description.'),
            array('album_id' => 5,
                'title' => 'Photo title 4',
                'file' => 'file.jpg',
                'description' => 'Photo 4 description.'),
            array('album_id' => 6,
                'title' => 'Photo title 5',
                'file' => 'file.jpg',
                'description' => 'Photo 5 description.'),
            array('album_id' => 7,
                'title' => 'Photo title 6',
                'file' => 'file.jpg',
                'description' => 'Photo 6 description.'),
            array('album_id' => 3,
                'title' => 'Photo title 7',
                'file' => 'file.jpg',
                'description' => 'Photo 7 description.'),
            array('album_id' => rand(2, 7),
                'title' => 'Photo title 8',
                'file' => 'file.jpg',
                'description' => 'Photo 8 description.'),
            array('album_id' => rand(2, 7),
                'title' => 'Photo title 9',
                'file' => 'file.jpg',
                'description' => 'Photo 9 description.'),
            array('album_id' => rand(2, 7),
                'title' => 'Photo title 10',
                'file' => 'file.jpg',
                'description' => 'Photo 10 description.'),
            array('album_id' => rand(2, 7),
                'title' => 'Photo title 11',
                'file' => 'file.jpg',
                'description' => 'Photo 11 description.'),
            array('album_id' => rand(2, 7),
                'title' => 'Photo title 12',
                'file' => 'file.jpg',
                'description' => 'Photo 12 description.'),
            array('album_id' => rand(2, 7),
                'title' => 'Photo title 13',
                'file' => 'file.jpg',
                'description' => 'Photo 13 description.'),
            array('album_id' => rand(2, 7),
                'title' => 'Photo title 14',
                'file' => 'file.jpg',
                'description' => 'Photo 14 description.'),
            array('album_id' => rand(2, 7),
                'title' => 'Photo title 15',
                'file' => 'file.jpg',
                'description' => 'Photo 15 description.'),
            array('album_id' => rand(2, 7),
                'title' => 'Photo title 16',
                'file' => 'file.jpg',
                'description' => 'Photo 16 description.'),
            array('album_id' => rand(2, 7),
                'title' => 'Photo title 17',
                'file' => 'file.jpg',
                'description' => 'Photo 17 description.'),
            array('album_id' => rand(2, 7),
                'title' => 'Photo title 18',
                'file' => 'file.jpg',
                'description' => 'Photo 18 description.'),
            array('album_id' => rand(2, 7),
                'title' => 'Photo title 19',
                'file' => 'file.jpg',
                'description' => 'Photo 19 description.'),

        );

        Photo::insert($photos);
        // Uncomment the below to run the seeder
        // DB::table('photos')->insert($photos);
    }

}
