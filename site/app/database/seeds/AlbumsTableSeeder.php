<?php

class AlbumsTableSeeder extends Seeder
{

    public function run()
    {
        // Uncomment the below to wipe the table clean before populating
        DB::table('albums')->truncate();

        $albums = array(
            array('title' => 'Album title 1',
                'user_id' => '1',
                'description' => 'Short description of album 1',
                'long_description' => 'Very very very very very long long long long long long description of album 1',
                'place' => 'Kaunas, Lithuania'),
            array('title' => 'Album title 2',
                'user_id' => '3',
                'description' => 'Short description of album 2',
                'long_description' => 'Very very very very very long long long long long long description of album 2',
                'place' => 'Vilnius, Lithuania'),
            array('title' => 'Album title 3',
                'user_id' => '2',
                'description' => 'Short description of album 3',
                'long_description' => 'Very very very very very long long long long long long description of album 3',
                'place' => 'Varena, Lithuania'),
            array('title' => 'Album title 4',
                'user_id' => '1',
                'description' => 'Short description of album 4',
                'long_description' => 'Very very very very very long long long long long long description of album 4',
                'place' => 'Kaunas, Lithuania'),
            array('title' => 'Album title 5',
                'user_id' => '2',
                'description' => 'Short description of album 5',
                'long_description' => 'Very very very very very long long long long long long description of album 5',
                'place' => 'Kaunas, Lithuania'),
            array('title' => 'Album title 6',
                'user_id' => '1',
                'description' => 'Short description of album 6',
                'long_description' => 'Very very very very very long long long long long long description of album 6',
                'place' => 'Kaunas, Lithuania'),
            array('title' => 'Album title 7',
                'user_id' => '3',
                'description' => 'Short description of album 7',
                'long_description' => 'Very very very very very long long long long long long description of album 7',
                'place' => 'Kaunas, Lithuania'),

        );

        Album::insert($albums);

        // Uncomment the below to run the seeder
        // DB::table('albums')->insert($albums);
    }

}
