Galerijos įrašymo instrukcijos
========================

Įrašinėjant su vagrant
----------------------------------

Atsidarome terminal ir vykdome šias komandas paeiliui:

    sudo apt-get install virtualbox, vagrant
    vagrant plugin install vagrant-hostsupdater
    sudo apt-get install nfs-kernel-server
    vagrant up
    vagrant provision
    vagrant ssh
    sudo apt-get install php5-xhprof
    exit
    vagrant reload
    vagrant ssh

Taip patenkame į virtualios mašinos SSH.

Prieš tęsdami atidarome failą /site/app/config/database.php ir susikonfigūruojame duombazę:

    'mysql' => array(
        'driver' => 'mysql',
        'host' => 'localhost',
        'database' => 'galerija',
        'username' => 'root',
        'password' => 'root',
        'charset' => 'utf8',
        'collation' => 'utf8_unicode_ci',
        'prefix' => '',
     ),

Toliau grįžtame į vagrant ssh ir darome komandas:

    cd /var/www/
    composer install
    php artisan migrate
    php artisan db:seed

Galerija turėtų veikti adresu nfqgalerija.dev

Administrtoriaus prisijungimai yra admin/pass