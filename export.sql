-- MySQL dump 10.13  Distrib 5.5.31, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: galerija
-- ------------------------------------------------------
-- Server version	5.5.31-0+wheezy1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `albums`
--

DROP TABLE IF EXISTS `albums`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `albums` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `main_photo_id` int(11) NOT NULL,
  `title` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `long_description` text COLLATE utf8_unicode_ci,
  `place` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `total_photos` int(10) unsigned NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `albums`
--

LOCK TABLES `albums` WRITE;
/*!40000 ALTER TABLE `albums` DISABLE KEYS */;
INSERT INTO `albums` VALUES (1,1,0,38,'<script>alert(\'xss\');</script>','Short description of album 1 <script>alert(\'xss\');</script>','Very very very very very long long long long long long description of album 1 <script>alert(\'xss\');</script>','Kaunas, Lithuania <script>alert(\'xss\');</script>',0,'0000-00-00 00:00:00','2013-12-05 17:10:06'),(3,2,0,0,'Album title 3','Short description of album 3','Very very very very very long long long long long long description of album 3','Varena, Lithuania',2,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(4,1,0,0,'Album title 4','Short description of album 4','Very very very very very long long long long long long description of album 4','Kaunas, Lithuania',2,'0000-00-00 00:00:00','2013-12-03 19:16:06'),(6,1,0,39,'Album title 6','Short description of album 6','Very very very very very long long long long long long description of album 6','Kaunas, Lithuania',2,'0000-00-00 00:00:00','2013-12-03 19:01:23');
/*!40000 ALTER TABLE `albums` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `photo_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `comment` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comments`
--

LOCK TABLES `comments` WRITE;
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
INSERT INTO `comments` VALUES (20,4,1,'nice i','2013-12-01 09:31:49','2013-12-01 09:31:49'),(59,2,1,'asdasd','2013-12-01 10:30:28','2013-12-01 10:30:28'),(61,2,1,'nicest','2013-12-01 12:17:20','2013-12-01 12:17:20'),(62,2,1,'I\'m sexyy and i know it','2013-12-01 12:17:36','2013-12-01 12:17:36'),(63,3,1,'testas','2013-12-03 19:42:34','2013-12-03 19:42:34'),(64,38,1,'<script>alert(\'xss\');</script>','2013-12-05 17:24:02','2013-12-05 17:24:02');
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `groups`
--

DROP TABLE IF EXISTS `groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `groups`
--

LOCK TABLES `groups` WRITE;
/*!40000 ALTER TABLE `groups` DISABLE KEYS */;
INSERT INTO `groups` VALUES (1,'user','0000-00-00 00:00:00','0000-00-00 00:00:00'),(2,'admin','0000-00-00 00:00:00','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `likes`
--

DROP TABLE IF EXISTS `likes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `likes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `photo_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `likes`
--

LOCK TABLES `likes` WRITE;
/*!40000 ALTER TABLE `likes` DISABLE KEYS */;
INSERT INTO `likes` VALUES (2,5,1,'0000-00-00 00:00:00'),(5,7,1,'0000-00-00 00:00:00'),(11,2,1,'0000-00-00 00:00:00'),(12,23,1,'0000-00-00 00:00:00'),(16,21,1,'0000-00-00 00:00:00');
/*!40000 ALTER TABLE `likes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES ('2013_10_06_171106_create_users_table',1),('2013_10_06_173650_create_albums_table',1),('2013_10_06_174551_create_photos_table',1),('2013_10_06_175231_add_unique_index_to_users',1),('2013_10_25_173326_create_categories_table',1),('2013_10_25_173407_create_groups_table',1),('2013_11_26_120040_create_comments_table',1),('2013_11_26_120058_create_likes_table',1),('2013_12_01_114526_create_tags_table',2),('2013_12_01_114559_create_photo_tag_table',2),('2013_12_01_115551_alter_total_photos_to_albums_table',3),('2013_12_01_120206_alter_total_likes_to_photos_table',4),('2013_12_01_120225_alter_total_comments_to_photos_table',4);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `photo_tag`
--

DROP TABLE IF EXISTS `photo_tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `photo_tag` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `photo_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `photo_tag_photo_id_tag_id_unique` (`photo_id`,`tag_id`)
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `photo_tag`
--

LOCK TABLES `photo_tag` WRITE;
/*!40000 ALTER TABLE `photo_tag` DISABLE KEYS */;
INSERT INTO `photo_tag` VALUES (62,3,14),(64,7,14),(2,30,1),(1,30,3),(4,31,1),(7,31,2),(6,31,4),(8,31,5),(9,31,6),(10,31,7),(11,32,8),(12,32,9),(13,32,10),(14,32,11),(15,32,12),(16,32,13),(17,32,14),(20,33,8),(24,33,9),(19,33,10),(23,33,11),(22,33,12),(21,33,13),(18,33,14),(27,34,8),(31,34,9),(26,34,10),(30,34,11),(29,34,12),(28,34,13),(25,34,14),(34,35,8),(38,35,9),(33,35,10),(37,35,11),(36,35,12),(35,35,13),(32,35,14),(41,36,8),(45,36,9),(40,36,10),(44,36,11),(43,36,12),(42,36,13),(39,36,14),(48,37,8),(52,37,9),(47,37,10),(51,37,11),(50,37,12),(49,37,13),(55,38,8),(59,38,9),(54,38,10),(58,38,11),(57,38,12),(56,38,13),(53,38,14),(60,39,6),(61,42,4),(63,43,1),(65,44,6),(66,44,15);
/*!40000 ALTER TABLE `photo_tag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `photos`
--

DROP TABLE IF EXISTS `photos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `photos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `album_id` int(11) NOT NULL,
  `title` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `file` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `taken` date NOT NULL,
  `total_likes` int(10) unsigned NOT NULL DEFAULT '0',
  `total_comments` int(10) unsigned NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `photos`
--

LOCK TABLES `photos` WRITE;
/*!40000 ALTER TABLE `photos` DISABLE KEYS */;
INSERT INTO `photos` VALUES (2,3,'Photo title 2','Photo 2 description.','file.jpg','0000-00-00',0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(3,1,'Photo title 3','Photo 3 description.','file.jpg','0000-00-00',0,1,'0000-00-00 00:00:00','2013-12-03 19:27:50'),(5,6,'Photo title 5','Photo 5 description.','file.jpg','0000-00-00',0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(7,3,'Photo title 7<script>alert(\'xss\');</script>','Photo 7 description.','file.jpg','0000-00-00',0,0,'0000-00-00 00:00:00','2013-12-05 17:18:57'),(8,6,'Photo title 8','Photo 8 description.','file.jpg','0000-00-00',0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(13,6,'Photo title 13','Photo 13 description.','file.jpg','0000-00-00',0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(14,6,'Photo title 14','Photo 14 description.','file.jpg','0000-00-00',0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(21,1,'testas','teasda','wallpaper-2872582-3.jpg','2013-12-25',1,0,'2013-12-01 12:25:59','2013-12-01 12:25:59'),(22,1,'Antras uploaded','asdasdasd','wallpaper-2872582-4.jpg','2013-10-02',0,0,'2013-12-01 17:12:18','2013-12-01 17:12:18'),(23,1,'Antras uploaded','asdasdasd','wallpaper-2872582-5.jpg','2013-10-02',0,0,'2013-12-01 17:28:24','2013-12-01 17:28:24'),(24,1,'Antras uploaded','asdasdasd','wallpaper-2872582-6.jpg','2013-10-02',0,0,'2013-12-01 17:30:52','2013-12-01 17:30:52'),(25,1,'Antras uploaded','asdasdasd','wallpaper-2872582-7.jpg','2013-10-02',0,0,'2013-12-01 17:31:23','2013-12-01 17:31:23'),(26,1,'Antras uploaded','asdasdasd','wallpaper-2872582-8.jpg','2013-10-02',0,0,'2013-12-01 17:32:13','2013-12-01 17:32:13'),(27,1,'Antras uploaded','asdasdasd','wallpaper-2872582-9.jpg','2013-10-02',0,0,'2013-12-01 17:32:24','2013-12-01 17:32:24'),(29,1,'Antras uploaded','asdasdasd','wallpaper-2872582-11.jpg','2013-10-02',0,0,'2013-12-01 17:35:12','2013-12-01 17:35:12'),(30,1,'Antras uploaded','asdasdasd','wallpaper-2872582-12.jpg','2013-10-02',0,0,'2013-12-01 17:37:32','2013-12-01 17:37:32'),(31,1,'asasdqwsdasd','asdasdasd','wallpaper-2872582-13.jpg','2013-10-02',0,0,'2013-12-01 17:38:32','2013-12-01 18:04:44'),(34,1,'I wish I can turn off my brain','I wish I can turn off my brain','wallpaper-1113694-3.jpg','2013-12-01',0,0,'2013-12-01 18:10:04','2013-12-01 18:10:04'),(35,1,'I wish I can turn off my brain','I wish I can turn off my brain','wallpaper-1113694-4.jpg','2013-12-01',0,0,'2013-12-01 18:10:07','2013-12-01 18:10:07'),(36,1,'I wish I can turn off my brain','I wish I can turn off my brain','wallpaper-1113694-5.jpg','2013-12-01',0,0,'2013-12-01 18:10:19','2013-12-01 18:10:19'),(37,1,'I wish I can turn off my brain','I wish I can turn off my brain','wallpaper-1113694-6.jpg','2013-12-01',0,0,'2013-12-01 18:10:22','2013-12-01 18:10:22'),(38,1,'I wish I can turn off my brain','I wish I can turn off my brain','wallpaper-1113694-7.jpg','2013-12-01',0,3,'2013-12-01 18:10:26','2013-12-01 18:10:26'),(39,6,'test','I wish I can turn off my brain','wallpaper-1113694-8.jpg','2013-10-25',0,0,'2013-12-03 19:01:23','2013-12-03 19:01:23'),(40,4,'I wish I can turn off my brain','I wish I can turn off my brain','wallpaper-1113694-9.jpg','2013-10-25',0,0,'2013-12-03 19:16:06','2013-12-03 19:16:06'),(41,4,'I wish I can turn off my brain','I wish I can turn off my brain','wallpaper-1113694-10.jpg','2013-10-25',0,0,'2013-12-03 19:22:49','2013-12-03 19:22:49'),(42,4,'I wish I can turn off my brain','I wish I can turn off my brain','wallpaper-1113694-11.jpg','2013-10-25',0,0,'2013-12-03 19:24:18','2013-12-03 19:24:18'),(43,4,'<script>alert(\'xss\');</script>','<script>alert(\'xss\');</script>','wallpaper-1113694-12.jpg','2013-10-23',0,0,'2013-12-03 19:44:55','2013-12-05 17:14:50'),(44,3,'asasdqwsdasd','Antras uploaded image','wallpaper-1113694-13.jpg','2013-10-25',0,1,'2013-12-05 21:04:04','2013-12-05 21:04:04');
/*!40000 ALTER TABLE `photos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tags`
--

DROP TABLE IF EXISTS `tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tag` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `tags_tag_unique` (`tag`),
  KEY `tags_tag_index` (`tag`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tags`
--

LOCK TABLES `tags` WRITE;
/*!40000 ALTER TABLE `tags` DISABLE KEYS */;
INSERT INTO `tags` VALUES (3,'aa'),(4,'aaaa'),(15,'asdwq'),(14,'brain'),(10,'can'),(7,'ever'),(1,'first'),(6,'geriausias'),(8,'I'),(13,'my'),(12,'off'),(5,'pirmas'),(2,'second'),(11,'turn'),(9,'wish');
/*!40000 ALTER TABLE `tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL DEFAULT '1',
  `username` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_username_unique` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,2,'admin','$2y$08$KI2H3uQUD8aWsjhcTZKpJeNTUEpIKQUVA9tbDro.jzUtr.3NuaTRu','0000-00-00 00:00:00','0000-00-00 00:00:00'),(5,1,'sexas1234','$2y$08$UhXVhlwh.mDLRb0sLhNVE.oV3GkV.yz6rLfM9OUVftr0hf3Tz2v7e','2013-11-30 16:10:45','2013-11-30 16:14:35'),(6,1,'testUser','$2y$08$UMXAW6165nuEhEWmVFTXoOxyyJpvvqtOtcC3bmwxLvRwEcfr8QvW6','2013-11-30 20:32:08','2013-11-30 20:32:08');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-12-06 14:17:22
