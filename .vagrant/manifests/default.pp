group { 'puppet': ensure => present }
Exec { path => [ '/bin/', '/sbin/', '/usr/bin/', '/usr/sbin/' ] }
File { owner => 0, group => 0, mode => 0644 }

class {'apt':
  always_apt_update => true,
}

Class['::apt::update'] -> Package <|
    title != 'python-software-properties'
and title != 'software-properties-common'
|>

apt::source { 'packages.dotdeb.org':
  location          => 'http://packages.dotdeb.org',
  release           => $lsbdistcodename,
  repos             => 'all',
  required_packages => 'debian-keyring debian-archive-keyring',
  key               => '89DF5277',
  key_server        => 'keys.gnupg.net',
  include_src       => true
}

if $lsbdistcodename == 'wheezy' {
  apt::source { 'packages.dotdeb.org-php55':
    location          => 'http://packages.dotdeb.org',
    release           => 'wheezy-php55',
    repos             => 'all',
    required_packages => 'debian-keyring debian-archive-keyring',
    key               => '89DF5277',
    key_server        => 'keys.gnupg.net',
    include_src       => true
  }
}

package { 'apache2-mpm-prefork':
  ensure => 'installed',
  notify => Service['apache'],
}

class { 'puphpet::dotfiles': }

package { [
    'build-essential',
    'vim',
    'curl',
    'git-core'
  ]:
  ensure  => 'installed',
}

class { 'apache': }

apache::dotconf { 'custom':
  content => 'EnableSendfile Off',
}

apache::module { 'rewrite': }
apache::module { 'php5': }

apache::vhost { 'nfqprojektas.dev':
  server_name   => 'nfqprojektas.dev',
  serveraliases => [
    'nfqprojektas.dev'
  ],
  docroot       => '/var/www/public/',
  port          => '80',
  env_variables => [
],
  priority      => '1',
}

apache::vhost { 'nfqgalerija.dev':
  server_name   => 'nfqgalerija.dev',
  serveraliases => [
    'www.nfqgalerija.dev'
  ],
  docroot       => '/var/www/public/',
  port          => '80',
  env_variables => [
],
  priority      => '1',
}

apache::vhost { 'xhprof.dev':
  server_name   => 'xhprof.dev',
  serveraliases => [
    'www.xhprof.dev'
  ],
  docroot       => '/var/www/vendor/facebook/xhprof/xhprof_html/',
  port          => '80',
  env_variables => [
],
  priority      => '1',
}

class { 'php':
  service             => 'apache',
  service_autorestart => true,
  module_prefix       => '',
}

php::module { 'php5-mysql': }
php::module { 'php5-cli': }
php::module { 'php5-curl': }
php::module { 'php5-gd': }
php::module { 'php5-intl': }
php::module { 'php5-mcrypt': }
php::module { 'php5-xhprof': }

class { 'php::devel':
  require => Class['php'],
}

class { 'php::pear':
  require => Class['php'],
}





class { 'xdebug':
  service => 'apache',
}

class { 'composer':
  require => Package['php5', 'curl'],
}

$custom_php_settings = [
    'xdebug.default_enable = 1',
    'xdebug.remote_autostart = 0',
    'xdebug.remote_connect_back = 1',
    'xdebug.max_nesting_level = 250',
    'xdebug.remote_enable = 1',
    'xdebug.remote_handler = "dbgp"',
    'xdebug.remote_port = 9000',
    'xdebug.idekey = "PHPSTORM"',
    'display_errors = On',
    'error_reporting = -1',
    'date.timezone = "Europe/Vilnius"',
    'xhprof.output_dir = "/var/www/xhprof/"'
]

puphpet::ini { 'php_apache':
  value   => $custom_php_settings,
  ini     => '/etc/php5/apache2/conf.d/custom_php.ini',
  notify  => Service['apache'],
  require => Class['php'],
}

puphpet::ini { 'php_cli':
  value   => $custom_php_settings,
  ini     => '/etc/php5/cli/conf.d/custom_php.ini',
  notify  => Service['apache'],
  require => Class['php'],
}

class { 'mysql::server':
  config_hash   => { 'root_password' => 'root', 'bind_address' => '10.10.21.10' }
}



